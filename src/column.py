# MIT License: https://gitlab.com/joeysbytes-python-snippets/tiny-column/-/blob/main/LICENSE?ref_type=heads
# Copyright (c) 2024 Joey Rockhold

from typing import Sequence


def column(items: Sequence[str], cols: int, spacing: int = 2) -> str:
    max_len=max([len(str(item))+spacing for item in items]); num_items=len(items); num_rows=(num_items+cols-1)//cols
    blank_cells = 0 if num_items % cols == 0 else cols - (num_items % cols); output = ""; cell: str
    for row in range(0, num_rows):
        idx = row
        for col in range(0, cols):
            if (col > cols - blank_cells - 1) or (idx > num_items - 1):
                if row == num_rows - 1: cell = f"{' ':<{max_len}}"
                else: cell = f"{items[idx]:<{max_len}}"; idx += num_rows - 1
            else: cell = f"{items[idx]:<{max_len}}"; idx += num_rows
            output += cell.rstrip() if col == cols - 1 else cell
        if row < num_rows - 1: output += "\n"
    return output
