# Tiny Column

This function will take a sequence of strings, and output them in a given number of columns, with user-specified
spacing between the columns.

## Parameters of column

| Parameter | Valid Values    | Default Value | Description                   |
|-----------|-----------------|---------------|-------------------------------|
| items     | Sequence of str | n/a           | Sequence of strings to output |
| cols      | >= 1            | n/a           | Number of columns to output   |
| spacing   | >= 0            | 2             | Spacing between columns       |

## Example 1

__Code:__

```python
items = ("Item 1", "Longer item", "Another item", "Some Item", "Item", "Food Item", "Computer Item")
print(column(items, 3))
```

__Output:__

```text
Item 1         Some Item      Food Item
Longer item    Item           Computer Item
Another item
```

## Example 2

__Code:__

```python
items = ("Item 1", "Longer item", "Another item", "Some Item", "Item", "Food Item", "Computer Item")
print(column(items, 3, 5))
```

__Output:__

```text
Item 1            Some Item         Food Item
Longer item       Item              Computer Item
Another item
```
