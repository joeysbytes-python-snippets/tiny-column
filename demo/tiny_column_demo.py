from column import column

animals = ("Cat",
           "Dog",
           "Parrot",
           "Snake",
           "Elephant",
           "Penguin",
           "Tyrannosaurus Rex",
           "Hippopotamus",
           "Tiger",
           "Lion",
           "Stegosaurus",
           "Cardinal",
           "Llama",
           "Sheep",
           "Donkey",
           "Pterodactyl",
           "Horse",
           "Bear Cub",
           "Wolverine",
           "Phoenix",
           # "test1",
           # "test2",
           # "test3",
           # "test4",
           # "test5",
           # "test6",

           )

print(column(animals, 8))
